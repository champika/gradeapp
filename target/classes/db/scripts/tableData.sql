INSERT INTO "user" (email,"password",user_type,username) VALUES 
('champ@test.com','$2a$10$nMhQeJ2kRAC//Jp7o2CooenNeYZbbvywVb758455TcMheFOD3kvMC','STUDENT','champ')
;

-- Auto-generated SQL script #202007071847
INSERT INTO public."assignment" (description,number_of_questions,title,owner_id)
	VALUES ('programming',3,'prog',1);


-- Auto-generated SQL script #202007071847
INSERT INTO public.question (description,assignment_id)
	VALUES ('java',1);
INSERT INTO public.question (description,assignment_id)
	VALUES ('c',1);
INSERT INTO public.question (description,assignment_id)
	VALUES ('python',1);


-- Auto-generated SQL script #202007071851
INSERT INTO public.assignment_result (is_done,student_id)
	VALUES (true,1);


-- Auto-generated SQL script #202007071852
INSERT INTO public.question_result (attempts,"result",time_spent,question_id)
	VALUES (1,'RIGHT',20,1);
INSERT INTO public.question_result (attempts,"result",time_spent,question_id)
	VALUES (1,'WRONG',15,2);
INSERT INTO public.question_result (attempts,"result",time_spent,question_id)
	VALUES (1,'PARTIAL',18,3);

INSERT INTO public.assignment_user (assignment_id,user_id) VALUES
(1,1)
;
