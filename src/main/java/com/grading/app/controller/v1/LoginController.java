package com.grading.app.controller.v1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.grading.app.data.v1.model.User;

@Controller
public class LoginController {
	@RequestMapping("/")
	public String index() {
		return "index.html";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

}
