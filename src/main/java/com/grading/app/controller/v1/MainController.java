package com.grading.app.controller.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.grading.app.data.v1.model.Assignment;
import com.grading.app.data.v1.model.AssignmentResult;
import com.grading.app.data.v1.vo.AssignmentVO;
import com.grading.app.service.v1.AssignmentService;

@Controller
@RequestMapping("${spring.data.rest.base-path}/v1/main")
public class MainController {
	
	@Autowired
	AssignmentService assignmentService;

	@GetMapping(path = "/assignments")
	public ResponseEntity<List<AssignmentVO>> listAssignments() {
		return ResponseEntity.ok(assignmentService.getAssignments());

	}

	@GetMapping(path = "/assignment_results/{id}")
	public ResponseEntity<AssignmentVO> getAssignmentByID(@PathVariable("id") Long id) {
		return ResponseEntity.ok(assignmentService.getAssignmentResultByID(id));
	}
}
