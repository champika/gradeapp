package com.grading.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradeAppWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradeAppWebApplication.class, args);
	}
}
