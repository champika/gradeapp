package com.grading.app.service.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grading.app.data.v1.model.Assignment;
import com.grading.app.data.v1.vo.AssignmentVO;
import com.grading.app.repository.v1.AssignmentRepository;
import com.grading.app.util.BoToVoConverter;

@Service
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	private AssignmentRepository assignmentRepository;

	@Autowired
	private UserService userService;

	@Override
	public AssignmentVO getAssignmentResultByID(long id) {
		Assignment assignment =  assignmentRepository.findByAssignmentID(id);
		if(assignment != null) {
			
			return  BoToVoConverter.assignmentToVOConverter.apply(assignment);
		}
		return new AssignmentVO();
	}

	@Override
	public List<AssignmentVO> getAssignments() {
		List<Assignment> assignments = userService.findByUsername(userService.getCurrentUser().getUsername()).getAssignments();
		if(assignments != null) {
			return assignments.stream().map(BoToVoConverter.assignmentToVOConverter).collect(Collectors.toList());
		}
		return new ArrayList<AssignmentVO>();
	}

}
