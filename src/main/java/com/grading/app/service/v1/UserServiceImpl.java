package com.grading.app.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.grading.app.data.v1.model.User;
import com.grading.app.repository.v1.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
    private UserRepository userRepository;
	
	@Override
	public org.springframework.security.core.userdetails.User getCurrentUser() {
		  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        if (authentication != null && authentication.getPrincipal() != null) {
	            return (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
	        }
	        return null;
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

}
