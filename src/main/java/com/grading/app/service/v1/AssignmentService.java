package com.grading.app.service.v1;

import java.util.List;

import com.grading.app.data.v1.vo.AssignmentVO;

public interface AssignmentService {

	public AssignmentVO getAssignmentResultByID(long id);
	public List<AssignmentVO>  getAssignments();
}
