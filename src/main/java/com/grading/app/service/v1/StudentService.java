package com.grading.app.service.v1;

import com.grading.app.data.v1.model.AssignmentResult;

public interface StudentService {

	public AssignmentResult getAssignmentResult(String assignment);
}
