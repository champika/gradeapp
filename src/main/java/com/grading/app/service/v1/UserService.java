package com.grading.app.service.v1;

import com.grading.app.data.v1.model.User;

public interface UserService {

	public org.springframework.security.core.userdetails.User getCurrentUser();
	public User findByUsername(String username);
}
