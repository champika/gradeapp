package com.grading.app.repository.v1;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grading.app.data.v1.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	public User findByUsername(String username);
}
