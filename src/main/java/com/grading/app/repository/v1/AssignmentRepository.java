package com.grading.app.repository.v1;


import org.springframework.data.jpa.repository.JpaRepository;

import com.grading.app.data.v1.model.Assignment;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {

	Assignment findByAssignmentID(long id);
}
