package com.grading.app.util;

import java.util.function.Function;
import java.util.stream.Collectors;

import com.grading.app.data.v1.model.Assignment;
import com.grading.app.data.v1.model.AssignmentResult;
import com.grading.app.data.v1.model.Question;
import com.grading.app.data.v1.model.QuestionResult;
import com.grading.app.data.v1.vo.AssignmentResultVO;
import com.grading.app.data.v1.vo.AssignmentVO;
import com.grading.app.data.v1.vo.QuestionResultVO;
import com.grading.app.data.v1.vo.QuestionVO;

public interface BoToVoConverter {
	
	Function<Assignment, AssignmentVO> assignmentToVOConverter = asm -> {
		AssignmentVO assignmentVO = null;
        if (asm != null) {
        	assignmentVO = new AssignmentVO();
            assignmentVO.setAssignmentID(asm.getAssignmentID());
            assignmentVO.setDescription(asm.getDescription());
            assignmentVO.setNumberOfQuestions(asm.getNumberOfQuestions());
            assignmentVO.setTitle(asm.getTitle());
            assignmentVO.setQuestions(asm.getQuestions().stream()
            		.map(BoToVoConverter.questionToVOConverter).collect(Collectors.toList()));
            assignmentVO.setAssignmentResults(asm.getAssignmentResults().stream()
            		.map(BoToVoConverter.assignmentResultToVOConverter).collect(Collectors.toList()));
        }
        return assignmentVO;
    };
    
    Function<AssignmentResult, AssignmentResultVO> assignmentResultToVOConverter = asm -> {
    	AssignmentResultVO vo = null;
        if (asm != null) {
        	vo = new AssignmentResultVO();
        	vo.setAssignmentResultID(asm.getAssignmentResultID());
        	vo.setDone(asm.isDone());
        }
        return vo;
    };
    
    Function<Question, QuestionVO> questionToVOConverter = qst -> {
    	QuestionVO questionVO = null;
        if (qst != null) {
        	questionVO = new QuestionVO();
            questionVO.setDescription(qst.getDescription());
            questionVO.setQuestionID(qst.getQuestionID());
            questionVO.setQuestionResults(qst.getQuestionResults().stream()
            		.map(BoToVoConverter.questionResultToVOConverter).collect(Collectors.toList()));
        }
        return questionVO;
    };
    
    Function<QuestionResult, QuestionResultVO> questionResultToVOConverter = qst -> {
    	QuestionResultVO vo = null;
        if (qst != null) {
        	vo = new QuestionResultVO();
        	vo.setAttempts(qst.getAttempts());
        	vo.setQuestionResultID(qst.getQuestionResultID());
        	vo.setResult(qst.getResult());
        	vo.setTimeSpent(qst.getTimeSpent());
        }
        return vo;
    };
}
