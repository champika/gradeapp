package com.grading.app.data.v1.vo;

public class AssignmentResultVO {
	private Long assignmentResultID;
	private boolean isDone;

	public Long getAssignmentResultID() {
		return assignmentResultID;
	}

	public void setAssignmentResultID(Long assignmentResultID) {
		this.assignmentResultID = assignmentResultID;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

}
