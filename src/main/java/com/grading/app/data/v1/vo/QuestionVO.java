package com.grading.app.data.v1.vo;

import java.util.List;

public class QuestionVO {
	private Long questionID;
	private String description;
	private List<QuestionResultVO> questionResults;

	public Long getQuestionID() {
		return questionID;
	}

	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<QuestionResultVO> getQuestionResults() {
		return questionResults;
	}

	public void setQuestionResults(List<QuestionResultVO> questionResults) {
		this.questionResults = questionResults;
	}

}
