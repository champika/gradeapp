package com.grading.app.data.v1.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.grading.app.data.v1.enumeration.UserType;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private Long userID;
	private String username;
	private String email;
	private String password;
	@Enumerated(EnumType.STRING)
	private UserType userType;

	@ManyToMany(mappedBy = "assignmentUsers")
	private List<Assignment> assignments;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "assignmentOwner", orphanRemoval = true)
	private List<Assignment> ownedAssignments;

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public List<Assignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	public List<Assignment> getOwnedAssignments() {
		return ownedAssignments;
	}

	public void setOwnedAssignments(List<Assignment> ownedAssignments) {
		this.ownedAssignments = ownedAssignments;
	}

}
