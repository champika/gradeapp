package com.grading.app.data.v1.vo;

import java.util.List;

import com.grading.app.data.v1.model.AssignmentResult;
import com.grading.app.data.v1.model.Question;

public class AssignmentVO {

	private Long assignmentID;
	private String title;
	private String description;
	private int numberOfQuestions;
	private List<QuestionVO> questions;
	private List<AssignmentResultVO> assignmentResults;

	public Long getAssignmentID() {
		return assignmentID;
	}

	public void setAssignmentID(Long assignmentID) {
		this.assignmentID = assignmentID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}

	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}

	public List<AssignmentResultVO> getAssignmentResults() {
		return assignmentResults;
	}

	public void setAssignmentResults(List<AssignmentResultVO> assignmentResults) {
		this.assignmentResults = assignmentResults;
	}

	public List<QuestionVO> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionVO> questions) {
		this.questions = questions;
	}

}
