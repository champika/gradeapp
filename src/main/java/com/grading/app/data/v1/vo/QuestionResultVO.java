package com.grading.app.data.v1.vo;

import com.grading.app.data.v1.enumeration.QResult;

public class QuestionResultVO {
	private int questionResultID;
	private long timeSpent;
	private QResult result;
	private int attempts;

	public int getQuestionResultID() {
		return questionResultID;
	}

	public void setQuestionResultID(int questionResultID) {
		this.questionResultID = questionResultID;
	}

	public long getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(long timeSpent) {
		this.timeSpent = timeSpent;
	}

	public QResult getResult() {
		return result;
	}

	public void setResult(QResult result) {
		this.result = result;
	}

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

}
