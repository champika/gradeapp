package com.grading.app.data.v1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.grading.app.data.v1.enumeration.QResult;

@Entity
@Table(name = "question_result")
public class QuestionResult {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "question_result_id")
	private int questionResultID;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "question_id")
	private Question question;
	private long timeSpent;
	@Enumerated(EnumType.STRING)
	private QResult result;
	private int attempts;

	
	public int getQuestionResultID() {
		return questionResultID;
	}
	public void setQuestionResultID(int questionResultID) {
		this.questionResultID = questionResultID;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public long getTimeSpent() {
		return timeSpent;
	}
	public void setTimeSpent(long timeSpent) {
		this.timeSpent = timeSpent;
	}
	public QResult getResult() {
		return result;
	}
	public void setResult(QResult result) {
		this.result = result;
	}
	public int getAttempts() {
		return attempts;
	}
	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}
	
}
