package com.grading.app.data.v1.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "assignment")
public class Assignment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "assignment_id")
	private Long assignmentID;
	private String title;
	private String description;
	private int numberOfQuestions;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	 @JoinTable(
	            name = "assignment_user",
	            joinColumns = {@JoinColumn(name = "assignment_id")},
	            inverseJoinColumns = {@JoinColumn(name = "user_id")}
	    )
	private List<User> assignmentUsers;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "assignment", orphanRemoval = true)
	private List<Question> questions;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "assignment", orphanRemoval = true)
	private List<AssignmentResult> assignmentResults;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	private User assignmentOwner;
	
	public Long getAssignmentID() {
		return assignmentID;
	}
	public void setAssignmentID(Long assignmentID) {
		this.assignmentID = assignmentID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}
	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public User getAssignmentOwner() {
		return assignmentOwner;
	}
	public void setAssignmentOwner(User assignmentOwner) {
		this.assignmentOwner = assignmentOwner;
	}
	public List<AssignmentResult> getAssignmentResults() {
		return assignmentResults;
	}
	public void setAssignmentResults(List<AssignmentResult> assignmentResults) {
		this.assignmentResults = assignmentResults;
	}
	public List<User> getAssignmentUsers() {
		return assignmentUsers;
	}
	public void setAssignmentUsers(List<User> assignmentUsers) {
		this.assignmentUsers = assignmentUsers;
	}
	
}
