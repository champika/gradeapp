CREATE TABLE "user" (
	user_id bigserial NOT NULL,
	email varchar(255) NULL,
	"password" varchar(255) NULL,
	user_type varchar(255) NULL,
	username varchar(255) NULL,
	CONSTRAINT user_pkey PRIMARY KEY (user_id)
);


CREATE TABLE "assignment" (
	assignment_id bigserial NOT NULL,
	description varchar(255) NULL,
	number_of_questions int4 NOT NULL,
	title varchar(255) NULL,
	owner_id int8 NULL,
	CONSTRAINT assignment_pkey PRIMARY KEY (assignment_id)
);
-- "assignment" foreign keys
ALTER TABLE "assignment" ADD CONSTRAINT fkshiygbkknmedxb4e4m4qetc0r FOREIGN KEY (owner_id) REFERENCES "user"(user_id);



CREATE TABLE assignment_result (
	assignment_result_id bigserial NOT NULL,
	is_done bool NOT NULL,
	student_id int8 NULL,
	CONSTRAINT assignment_result_pkey PRIMARY KEY (assignment_result_id)
);
-- assignment_result foreign keys
ALTER TABLE assignment_result ADD CONSTRAINT fkordb40fv4bfnenoyc5991jcb0 FOREIGN KEY (student_id) REFERENCES assignment(assignment_id);



CREATE TABLE assignment_user (
	assignment_id int8 NOT NULL,
	user_id int8 NOT NULL
);

-- assignment_user foreign keys
ALTER TABLE assignment_user ADD CONSTRAINT fkc4269trrinaws16kplrrvjclj FOREIGN KEY (user_id) REFERENCES "user"(user_id);
ALTER TABLE assignment_user ADD CONSTRAINT fkcynf9fxoyk2vr28ouywq37xm2 FOREIGN KEY (assignment_id) REFERENCES assignment(assignment_id);



CREATE TABLE question (
	question_id bigserial NOT NULL,
	description varchar(255) NULL,
	assignment_id int8 NULL,
	CONSTRAINT question_pkey PRIMARY KEY (question_id)
);


-- question foreign keys

ALTER TABLE question ADD CONSTRAINT fkfl6mn0nu3cyhbo2ihco9hm61 FOREIGN KEY (assignment_id) REFERENCES assignment(assignment_id);





CREATE TABLE question_result (
	question_result_id serial NOT NULL,
	attempts int4 NOT NULL,
	"result" varchar(255) NULL,
	time_spent int8 NOT NULL,
	question_id int8 NULL,
	CONSTRAINT question_result_pkey PRIMARY KEY (question_result_id)
);


-- question_result foreign keys

ALTER TABLE question_result ADD CONSTRAINT fkk2exlunxduwmfm2pl4mot27jk FOREIGN KEY (question_id) REFERENCES question(question_id);






