package com.grading.app.test;

import com.grading.app.data.v1.model.Assignment;
import com.grading.app.data.v1.model.User;
import com.grading.app.repository.v1.AssignmentRepository;
import com.grading.app.repository.v1.UserRepository;
import com.grading.app.service.v1.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {
    @Mock
    private UserRepository userRepository;

    @Test
    public void getUserByUserNameTest()
    {
        User user1 = new User();
        user1.setUserID(1l);
        user1.setUsername("champ");
        user1.setAssignments(new ArrayList<Assignment>());
        user1.setEmail("champ@test.lk");
        when(userRepository.findByUsername("champ")).thenReturn(user1);
        User user = userRepository.findByUsername("champ");
        assertEquals("champ", user.getUsername());
    }
}
