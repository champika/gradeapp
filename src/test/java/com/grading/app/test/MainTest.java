package com.grading.app.test;

import com.grading.app.data.v1.model.Assignment;
import com.grading.app.data.v1.model.User;
import com.grading.app.data.v1.vo.AssignmentVO;
import com.grading.app.repository.v1.AssignmentRepository;
import com.grading.app.repository.v1.UserRepository;
import com.grading.app.service.v1.AssignmentService;
import com.grading.app.service.v1.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainTest {

    @Mock
    private AssignmentService assignmentService;

    @Test
    public void getAssignmentsTest()
    {
        List<AssignmentVO> assignmentList = new ArrayList<>();
        assignmentList.add(new AssignmentVO());
        assignmentList.add(new AssignmentVO());
        assignmentList.add(new AssignmentVO());
        when(assignmentService.getAssignments()).thenReturn(assignmentList);
        List<AssignmentVO> list = assignmentService.getAssignments();
        assertEquals(3, list.size());
    }

    @Test
    public void getAssignmentsByIdTest()
    {
        AssignmentVO assignmentVO = new AssignmentVO();
        assignmentVO.setAssignmentID(1l);
        assignmentVO.setDescription("test");
        assignmentVO.setNumberOfQuestions(10);
        assignmentVO.setTitle("Tester");
        when(assignmentService.getAssignmentResultByID(1)).thenReturn(assignmentVO);
        AssignmentVO assi = assignmentService.getAssignmentResultByID(1);
        assertEquals("Tester", assi.getTitle());
    }
}
